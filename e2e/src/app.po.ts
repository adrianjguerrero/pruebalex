import { browser, by, element } from 'protractor';

export class AppPage {
  getListClientes() {
    return element.all(by.css('table tbody > tr'));
  }
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }
}

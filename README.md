# Pruebalex


stage instalar, decimos q es instal, q instale
el artifacts es el producto q genera este stage
entonces decimos que lo cachee para el proximo stage
```
install:
  stage: install
  script: 
    - npm install
  artifacts:
    expire_in: 1h
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/
```


fase de test
```
tests:
  stage: test
  variables:
    CHROME_BIN: google-chrome
  dependencies:
    - install //aqui decimos q depende del stage script
  before_script:
  //antes del script, instalamos chrome headless
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb https://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
  //ejecutamos el comando que definimos anteriormente
    - npm run test:ci
  coverage: '/Statements.*?(\d+(?:\.\d+)?)%/'
  //el comando nos genera un reporte de cobertura, necesitamos escojer alguno de esos 
  //valores para que nos diga gitlab q tanta corbertura tenemos, aqui tomamos statements
```
<!-- ya hemos aprobado el install y test, y ya hicimos merge request, y la rama quedo borrada en gitlab -->

```
build:
  stage: build
  variables:
    BUILD_CONFIG: 'production'
  dependencies:
    - install
  script:
    - npm run build:ci
  artifacts:
  //nuevamente, aqui decimos que esto genera la carpeta dist
    expire_in: 1h
    paths:
      - dist/
  only:
    - master

```

```
"build:ci": "ng build -c=$BUILD_CONFIG --baseHref=/pruebalex/",
<!-- -c=$BUILD_CONFIG usamos la variable que definimos en el stage build -->
<!-- --baseHref=/pruebalex/ cambiamos la carpeta root -->
<!--   only:    - master con esto decimos que solo corra al hacer push a master -->
```

```
pages:
<!-- para hacer deploy en glpages ajuro la tarea de deploy se tiene q llamar pages -->
  stage: deploy
  dependencies:
    - build
  script:
    - mkdir public
    - mv ./dist/pruebalex/* ./public/
    <!-- llevamos todo lo q esta en esa carpeta q generamos, a public -->
    - cp ./public/index.html ./public/404.html
    <!-- esto es solo por seguridad, cualquier 404 que lleve al mismo index.html -->
  artifacts:
    paths:
      - public/
  environment:
  <!-- aqui decmos que esto pertenece al enviroment de produccion, solo para tener todo mas organizado en gitlab -->
    name: production
  only:
    - master
```

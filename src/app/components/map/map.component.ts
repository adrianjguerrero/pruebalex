import { Component, OnInit, Input } from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() latitude: number;
  @Input() longitude: number;
  @Input() name: string;
  private map;
  token = 'pk.eyJ1IjoiYWRyaWFuamd1ZXJyZXJvIiwiYSI6ImNrN2YzdmdycjF3ZTIzZnFybXBnajN1MTcifQ.XElH7tju0w2GYTcYoA4dfA';

  constructor() {}

  ngOnInit(): void {

    this.initMap();
  }

  private initMap(): void {

    const icon = {
      icon: L.icon({
        iconSize: [ 25, 41 ],
        iconAnchor: [ 13, 0 ],
        iconUrl: './assets/marker-icon.png',
        shadowUrl: './assets/marker-shadow.png'
      })
    };
    this.map = L.map('map', {
      center: [this.latitude - 50, this.longitude - 50],
      zoom: 3
    });
    const tiles = L.tileLayer(
      `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${this.token}`,
      {
        attribution:
          // tslint:disable-next-line:max-line-length
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
          maxZoom: 18,
          id: 'mapbox/streets-v11',
          tileSize: 512,
          zoomOffset: -1,
          accessToken: this.token,
      }
    ).addTo(this.map);

    const marker = L.marker([this.latitude, this.longitude], icon);

    marker.bindPopup(`Ubicación de<b>${this.name}</b>.`).openPopup();
    marker.addTo(this.map);



    tiles.addTo(this.map);
  }
}
